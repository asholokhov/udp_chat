#include "TSettings.h"

#include <QSettings>
#include <QFile>
#include <QDebug>

extern GlobalChat::Settings m_global_settings;

TSettings::TSettings(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}

void TSettings::loadSettings() {
    if (QFile("conf.dat").exists()) {
        emit logEvent("Configuration file was founded. Loading settings from conf.dat.");

        QSettings sett(QString("conf.dat"), QSettings::NativeFormat, this);

        sett.beginGroup("Global");
            m_global_settings.nickname  = sett.value("Nickname").toString();
            m_global_settings.hostname  = sett.value("Hostname").toString();
            m_global_settings.host_port = sett.value("Port").toInt();

        emit logEvent("Nickname: " + m_global_settings.nickname);
        emit logEvent("Host: " + m_global_settings.hostname);
        emit logEvent("Port: " + QString::number(m_global_settings.host_port));
    } else {
        m_global_settings.nickname  = "Nickname";
        m_global_settings.hostname  = "127.0.0.1";
        m_global_settings.host_port = 7755;
    }

    lineEdit_Nickname->setText(m_global_settings.nickname);
    lineEdit_Hostname->setText(m_global_settings.hostname);
    lineEdit_Port->setText(QString::number(m_global_settings.host_port));
}

void TSettings::on_pushButton_Save_clicked() {
    m_global_settings.nickname  = lineEdit_Nickname->text();
    m_global_settings.hostname  = lineEdit_Hostname->text();
    m_global_settings.host_port = lineEdit_Port->text().toInt();

    QSettings sett("conf.dat", QSettings::NativeFormat, this);

    sett.beginGroup("Global");
        sett.setValue("Nickname", m_global_settings.nickname);
        sett.setValue("Hostname", m_global_settings.hostname);
        sett.setValue("Port",     m_global_settings.host_port);
    sett.endGroup();

    emit logEvent("Settings updated.");
    emit logEvent("Host: " + m_global_settings.hostname);
    emit logEvent("Port: " + QString::number(m_global_settings.host_port));

    accept();
}

void TSettings::on_pushButton_Cancel_clicked() {
    reject();
}
