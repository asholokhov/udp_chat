#ifndef TSETTINGS_H
#define TSETTINGS_H

#include <QDialog>
#include "ui_settings.h"
#include "includes.h"

class TSettings : public QDialog, private Ui::SettingsDialog
{
    Q_OBJECT
public:
    explicit TSettings(QWidget *parent = 0);
    void loadSettings();
    
signals:
    void logEvent(QString logEvent);

private slots:
    void on_pushButton_Save_clicked();
    void on_pushButton_Cancel_clicked();
    
};

#endif // TSETTINGS_H
