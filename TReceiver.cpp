#include "TReceiver.h"

extern GlobalChat::Settings m_global_settings;

TReceiver::TReceiver(QObject *parent) :
    QObject(parent)
{
    m_udpReciever = new QUdpSocket(this);
    connect(m_udpReciever, SIGNAL(readyRead()), this, SLOT(processPendingDatagrams()));

}

void TReceiver::listen() {
    if (m_udpReciever->bind(m_global_settings.host_port,
                            QUdpSocket::DefaultForPlatform))
        emit logEvent("Start listening " + QString(m_global_settings.host_port) + " port");
}

void TReceiver::processPendingDatagrams() {
    emit logEvent(QDateTime::currentDateTime().toString("[hh:mm:ss] -") +
                  " datagram recieved (size: " + QString::number(m_udpReciever->pendingDatagramSize()));

    while (m_udpReciever->hasPendingDatagrams()) {
        GlobalChat::DatagramPacket *packet;

        int  data_size = m_udpReciever->pendingDatagramSize();
        char data[data_size];
        m_udpReciever->readDatagram(data, data_size);

        packet = reinterpret_cast<GlobalChat::DatagramPacket*>(&data);
        m_received_ids[packet->id]++;

        if (m_received_ids[packet->id] == 1) // else - duplicates
              emit newMessage(QString(packet->message), QString(packet->nick));
    }

}
