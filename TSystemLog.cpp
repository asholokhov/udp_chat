#include "TSystemLog.h"
#include <QDebug>

TSystemLog::TSystemLog(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
}

void TSystemLog::setLogMessage(QString msg) {
    log_textBrowser->setText(msg);
}

void TSystemLog::on_pushButton_ok_clicked() {
     accept();
}
