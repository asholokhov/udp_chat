#ifndef TSYSTEMLOG_H
#define TSYSTEMLOG_H

#include <QObject>
#include "ui_log.h"

class TSystemLog : public QDialog, private Ui::JournalDialog
{
    Q_OBJECT
public:
    explicit TSystemLog(QWidget *parent = 0);
    void setLogMessage(QString msg);

private slots:
    void on_pushButton_ok_clicked();
};

#endif // TSYSTEMLOG_H
