#include "TSender.h"

extern GlobalChat::Settings m_global_settings;

TSender::TSender(QObject *parent) :
    QObject(parent),
    m_message_id(0)
{
    m_udpSender = new QUdpSocket(this);
}


void TSender::sendMessage(QString msg) {
    GlobalChat::DatagramPacket packet(m_message_id, m_global_settings.nickname.toStdString().c_str(),
                                      msg.toStdString().c_str());

    for (int i = 0; i < SEND_PACKETS_COUNT; i++) {
        m_udpSender->writeDatagram(reinterpret_cast<char*>(&packet),
                                   sizeof(packet),
                                   QHostAddress(m_global_settings.hostname),
                                   m_global_settings.host_port);
    }

    m_message_id++;
    emit newMessage(msg);
}
