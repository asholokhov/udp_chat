#include "TMainWindow.h"

#include "TSystemLog.h"
#include "TSettings.h"

extern GlobalChat::Settings m_global_settings;

TMainWindow::TMainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_key_filter(new KeyPressFilter())
{
    setupUi(this);

    m_udpSender   = new TSender(this);
    m_udpReciever = new TReceiver(this);

    connect(m_udpSender, SIGNAL(newMessage(QString)), this, SLOT(newMessageSent(QString)));
    connect(m_udpReciever, SIGNAL(newMessage(QString, QString)), this, SLOT(newMessageReceived(QString, QString)));

    connect(m_udpSender, SIGNAL(logEvent(QString)), this, SLOT(pushLogEvent(QString)));
    connect(m_udpReciever, SIGNAL(logEvent(QString)), this, SLOT(pushLogEvent(QString)));

    connect(m_key_filter, SIGNAL(enterPressed()), this, SLOT(processEnterPressed()));

    m_udpReciever->listen();
    m_sysJournal = "";

    sourceMsg_textEdit->installEventFilter(m_key_filter);
    messagesBrowser->setText("<b>Today: " + QDateTime::currentDateTime().toString("dd.MM.yy") + "</b><hr />");
}

TMainWindow::~TMainWindow() {
    delete m_key_filter;
    delete m_udpReciever;
    delete m_udpSender;
}

void TMainWindow::processEnterPressed() {
    on_pushButton_Send_clicked();
}

void TMainWindow::scrollChatToBottom() const {
    QScrollBar *sb = messagesBrowser->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void TMainWindow::pushLogEvent(QString message) {
    m_sysJournal += message + '\n';
}

void TMainWindow::on_actionSettings_triggered() {
    TSettings _settings(this);
    _settings.setModal(true);
    QObject::connect(dynamic_cast<QObject*>(&_settings), SIGNAL(logEvent(QString)), this, SLOT(pushLogEvent(QString)));
    _settings.loadSettings();
    _settings.exec();
}

void TMainWindow::on_actionAbout_triggered() {
    QMessageBox::information(this, tr("About"), tr("Sholokhov Artem, SP-01\nSPb SUT 2013"), QMessageBox::Ok);
}

void TMainWindow::on_actionShow_system_log_triggered() {
    TSystemLog _log(this);
    _log.setLogMessage(m_sysJournal);
    _log.setModal(true);
    _log.exec();
}

void TMainWindow::on_actionExit_triggered() {
    QApplication::exit(0);
}

void TMainWindow::on_pushButton_Send_clicked() {
    pushLogEvent("Send message button clicked");
    QString message = sourceMsg_textEdit->toPlainText();
    try {
        m_udpSender->sendMessage(message);
        sourceMsg_textEdit->clear();
    } catch(...) {
        pushLogEvent("Error while sending message: " + message);
    }
}

void TMainWindow::newMessageSent(QString message) {
    QString oldText = messagesBrowser->toHtml();
    QString timeText = "<font color='red'><i>[" + m_global_settings.nickname + "]" +
            QDateTime::currentDateTime().toString("[hh:mm:ss]: ") + "</i></font>\n";
    messagesBrowser->setText(oldText + timeText + message);
    scrollChatToBottom();
}

void TMainWindow::newMessageReceived(QString message, QString nick) {
    QString oldText = messagesBrowser->toHtml();
    QString timeText = "<font color='green'><i>[" + nick + "]" +
            QDateTime::currentDateTime().toString("[hh:mm:ss]: ") + "</i></font>\n";
    messagesBrowser->setText(oldText + timeText + message);
    scrollChatToBottom();
}
