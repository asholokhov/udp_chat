#ifndef TSENDER_H
#define TSENDER_H

#include "includes.h"

#define SEND_PACKETS_COUNT 4

class TSender : public QObject
{
    Q_OBJECT
public:
    TSender(QObject *parent = 0);
    void sendMessage(QString msg);

signals:
    void newMessage(QString message);
    void logEvent(QString logEvent);

private:
    QUdpSocket *m_udpSender;
    QString     m_hostname;
    int         m_port;
    int         m_message_id;
};

#endif // TSENDER_H
