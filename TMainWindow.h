#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <QMainWindow>
#include <QEvent>
#include <QKeyEvent>
#include <QTextEdit>
#include "ui_main.h"
#include "includes.h"

class KeyPressFilter : public QObject
{
    Q_OBJECT

signals:
    void enterPressed();

protected:
    bool eventFilter(QObject *obj, QEvent *event) {
        if (obj != dynamic_cast<QTextEdit*>(obj))
            return QObject::eventFilter(obj, event);
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return) {
                emit enterPressed();
                return true;
            }
            else
                return QObject::eventFilter(obj, event);
        } else {
            // standard event processing
            return QObject::eventFilter(obj, event);
        }
    }
};


class TMainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT
public:
    explicit TMainWindow(QWidget *parent = 0);
    ~TMainWindow();
    
private slots:
    void on_pushButton_Send_clicked();
    void on_actionSettings_triggered();
    void on_actionShow_system_log_triggered();
    void on_actionExit_triggered();
    void on_actionAbout_triggered();

    void newMessageReceived(QString message, QString nick);
    void newMessageSent(QString message);
    void pushLogEvent(QString message);
    void processEnterPressed();

private:
    TSender        *m_udpSender;
    TReceiver      *m_udpReciever;
    KeyPressFilter *m_key_filter;
    QString         m_sysJournal;

    void scrollChatToBottom() const;
};

#endif // TMAINWINDOW_H
