QT += network

SOURCES += \
    main.cpp \
    TSender.cpp \
    TMainWindow.cpp \
    TSystemLog.cpp \
    TSettings.cpp \
    TReceiver.cpp

HEADERS += \
    TSender.h \
    includes.h \
    TMainWindow.h \
    TSystemLog.h \
    TSettings.h \
    TReceiver.h

FORMS += \
    main.ui \
    settings.ui \
    log.ui
