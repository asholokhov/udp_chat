#ifndef INCLUDES_H
#define INCLUDES_H

#define DEFAULT_RECIEVER_PORT 7755
#define DEFAULT_RECIEVER_HOST "127.0.0.1"

#include <QObject>
#include <QUdpSocket>
#include <QDebug>
#include <QString>
#include <QApplication>
#include <QDateTime>
#include <QScrollBar>
#include <QMessageBox>

#include <string.h>

namespace GlobalChat {
    struct Settings {
        QString nickname;
        QString hostname;
        int     host_port;
    };

    struct DatagramPacket {
        int  id;
        char nick[20];
        char message[100];

        DatagramPacket() {}
        DatagramPacket(const int _id, const char* _nick, const char* _msg) :
            id(_id) {
            strcpy(nick, _nick);
            strcpy(message, _msg);
        }
    };
}

#include "TSender.h"
#include "TReceiver.h"
#include "TSettings.h"

#endif // INCLUDES_H
