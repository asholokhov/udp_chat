#include "TMainWindow.h"
#include "TSettings.h"

#include <QTextCodec>

GlobalChat::Settings m_global_settings;

int main(int argc, char* argv[]) {
    QApplication _qapp(argc, argv);

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF8"));

    QCoreApplication::setOrganizationName("Sholokhov Artem");
    QCoreApplication::setApplicationName("UDP Based chat (uChat)");

    TSettings _set(0);
    _set.loadSettings();

    TMainWindow _wnd;
    _wnd.show();

    return (_qapp.exec());
}
