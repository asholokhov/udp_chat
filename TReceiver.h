#ifndef TRECEIVER_H
#define TRECEIVER_H

#include "includes.h"
#include <QMap>

class TReceiver : public QObject
{
    Q_OBJECT
public:
    TReceiver(QObject *parent = 0);
    void listen();

signals:
    void newMessage(QString message, QString nick);
    void logEvent(QString logEvent);

private slots:
    void processPendingDatagrams();

private:
    int              m_listening_port;
    QUdpSocket      *m_udpReciever;
    QMap <int, int>  m_received_ids;
};

#endif // TRECEIVER_H
